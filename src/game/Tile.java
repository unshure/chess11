package game;

/** Stores all information for a tile
 * 
 * @author Nick Clegg
 * @author Kevin Wu
 * @version 1.0
 *
 */
public class Tile {
	
	private boolean isOccupied;
	private int x_coordinate;
	private int y_coordinate;
	
	public boolean isOccupied() {
		return isOccupied;
	}
	public void setOccupied(boolean isOccupied) {
		this.isOccupied = isOccupied;
	}
	public int getX_coordinate() {
		return x_coordinate;
	}
	public void setX_coordinate(int x_coordinate) {
		this.x_coordinate = x_coordinate;
	}
	public int getY_coordinate() {
		return y_coordinate;
	}
	public void setY_coordinate(int y_coordinate) {
		this.y_coordinate = y_coordinate;
	}
	
}
