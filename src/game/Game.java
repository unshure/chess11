package game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import pieces.King;
import pieces.Move;
import pieces.Pawn;
import pieces.Piece;

/** Holds the rules for pieces for the game of chess
 * 
 * @author Nick Clegg
 * @author Kevin Wu
 * @version 1.0
 *
 */
public class Game {
	
	private static int currentTurnNumber = 1;
	private boolean drawRequested = false;
	
	public boolean isDrawRequested() {
		return drawRequested;
	}

	public void setDrawRequested(boolean drawRequested) {
		this.drawRequested = drawRequested;
	}
	
	//Janky stuff
	static int black_king_x;
	static int black_king_y;
	static int white_king_x;
	static int white_king_y;
	
	
	/** Enum for different game outcomes
	 * 
	 *
	 */
	public enum GameState{
		NORMAL(0), WHITE_WINS_C(1), BLACK_WINS_C(2), STALEMATE(3), DRAW(4), WHITE_WINS_R(5), BLACK_WINS_R(6);
		
		@SuppressWarnings("unused")
		private int value;
		private GameState(int value) {
			this.value = value;
		}
	}
	/** Converts coordinates from "letterNumber" format to xy position
	 * 
	 * @param c Character to convert to coordinate
	 * @return Converted Coordinate
	 */
	public int convertCoordinates(char c) {
		int numberCoordinate = 0;
		switch(c) {
			case '1':
			case 'a':
				numberCoordinate = 1;
				break;
			case '2':
			case 'b':
				numberCoordinate = 2;
				break;
			case '3':
			case 'c':
				numberCoordinate = 3;
				break;
			case '4':
			case 'd':
				numberCoordinate = 4;
				break;
			case '5':
			case 'e':
				numberCoordinate = 5;
				break;
			case '6':
			case 'f':
				numberCoordinate = 6;
				break;
			case '7':
			case 'g':
				numberCoordinate = 7;
				break;
			case '8':
			case 'h':
				numberCoordinate = 8;
				break;
			default:
				//Should not get to this point
				System.out.println("Illegal move, try again. Invalid coordinates");
		}
		return numberCoordinate;
	}
	
	/** Checks if the Target Move is a Valid Move
	 * 
	 * @param targetMove Move to be checked
	 * @param validMovesList List of all valid moves
	 * @return Target move if it is valid, false if move is invalid
	 */
	public Move searchMove(Move targetMove, ArrayList<Move> validMovesList) {
		for(int i=0; i<validMovesList.size(); i++) {
			Move validMove = validMovesList.get(i);
			if(targetMove.getCurrent_x() == validMove.getCurrent_x()
					&& targetMove.getCurrent_y() == validMove.getCurrent_y()
					&& targetMove.getNew_x() == validMove.getNew_x()
					&& targetMove.getNew_y() == validMove.getNew_y()) {
				return validMove;
			}
		}
		return null;
	}
	/** Appends the current game move to the end of the game log
	 * 
	 * @param gameLog List of all previous game moves
	 * @param move Current Move
	 * @return Updated game log
	 */
	public ArrayList<Turn> updateGameLog(ArrayList<Turn> gameLog, Move move){
		int move_turnNumber = currentTurnNumber;
		Turn nextTurn = new Turn(move, move_turnNumber);
		gameLog.add(nextTurn);
		return gameLog;
	}
	/** Rules for all different kinds of moves
	 * 
	 * @param moveString String input into the terminal to be changed to xy coordinated
	 * @param validMovesList List of all valid moves
	 * @param board Current Board
	 * @param drawRequested Checks if a draw has been requested
	 * @param gameLog All previous moves
	 */
	public void parseMove(String moveString, ArrayList<Move> validMovesList, Board board, 
			boolean drawRequested, ArrayList<Turn> gameLog) {
		
		Tile[][] chessBoard = board.getChessBoard();
		Piece[] piecesArray = board.getPiecesArray();
		//System.out.println("This is the len of string: " + moveString.length() + "\n");
		if(moveString.length() < 5) {
			System.out.println("Illegal move, try again");
		}else if((moveString.charAt(0) >= 'a' && moveString.charAt(0) <= 'h')
				&& (moveString.charAt(1) >= '1' && moveString.charAt(1) <= '8')
				&& moveString.charAt(2) == ' '
				&& (moveString.charAt(3) >= 'a' && moveString.charAt(3) <= 'h')
				&& (moveString.charAt(4) >= '1' && moveString.charAt(4) <= '8')) {
			//System.out.println("Proper Format for move\n");
			if(moveString.length() == 5) {
				//System.out.println("regular move\n");
				//Means it is a regular move
				int current_x = convertCoordinates(moveString.charAt(0));
				int current_y = convertCoordinates(moveString.charAt(1));
				int new_x = convertCoordinates(moveString.charAt(3));
				int new_y = convertCoordinates(moveString.charAt(4));
				//Just create a default move with move_type = 0
				Move inputMove = new Move(current_x, current_y, new_x, new_y, 0);
				if(searchMove(inputMove, validMovesList) != null) {
					//Means move is found in the valid moves list
					//Next check the move_type
					Move nextMove = searchMove(inputMove, validMovesList);
					Piece piece = board.getPieceFromCoordinate(current_x, current_y, piecesArray);
					switch(nextMove.getMove_type()) {
						case 0:
							//Regular move
							//First find the piece you are trying to move
							piece.move(chessBoard, piecesArray, nextMove);
							updateGameLog(gameLog, nextMove);
							System.out.println();
							board.printBoard(board.getChessBoard(), board.getPiecesArray());
							System.out.println();
							this.setDrawRequested(false);
							currentTurnNumber++;
							break;
						case 1:
							//Capture move
							//Check if pawn first
							if(piece instanceof Pawn) {
								//If piece is an instance of pawn, check if move goes to rank 1 or rank 8
								if(nextMove.getNew_y() == 1 || nextMove.getNew_y() == 8) {
									Pawn pawn = (Pawn) piece;
									pawn.promoteCapture(chessBoard, piecesArray, nextMove, 'Q');
									updateGameLog(gameLog, nextMove);
								}else {
									piece.capture(chessBoard, piecesArray, nextMove);
									updateGameLog(gameLog, nextMove);
								}
							}else {
								piece.capture(chessBoard, piecesArray, nextMove);
								updateGameLog(gameLog, nextMove);
							}
							System.out.println();
							board.printBoard(board.getChessBoard(), board.getPiecesArray());
							System.out.println();
							this.setDrawRequested(false);
							currentTurnNumber++;
							break;
						case 2:
							//Castling short for king, promotion for pawn
							if(piece instanceof King) {
								King king = (King) piece;
								king.castle_short(chessBoard, piecesArray, nextMove);
								updateGameLog(gameLog, nextMove);
								System.out.println();
								board.printBoard(board.getChessBoard(), board.getPiecesArray());
								System.out.println();
								this.setDrawRequested(false);
								currentTurnNumber++;
							}else if(piece instanceof Pawn){
								Pawn pawn = (Pawn) piece;
								//Since string length is only 5, no promotion is indicated
								//Therefore it is assumed to be promoted to a queen
								pawn.promoteRegular(chessBoard, piecesArray, nextMove, 'Q');
								updateGameLog(gameLog, nextMove);
								System.out.println();
								board.printBoard(board.getChessBoard(), board.getPiecesArray());
								System.out.println();
								this.setDrawRequested(false);
								currentTurnNumber++;
							}else {
								//Should not reach this branch
								System.out.println("Why is move type 3? It's not a king nor a pawn.");
							}
							break;
						case 3:
							//Castling long for king, en passant for pawn
							if(piece instanceof King) {
								King king = (King) piece;
								king.castle_long(chessBoard, piecesArray, nextMove);
								updateGameLog(gameLog, nextMove);
								System.out.println();
								board.printBoard(board.getChessBoard(), board.getPiecesArray());
								System.out.println();
								this.setDrawRequested(false);
								currentTurnNumber++;
							}else if(piece instanceof Pawn) {
								Pawn pawn = (Pawn) piece;
								pawn.en_passant(chessBoard, piecesArray, nextMove);
								updateGameLog(gameLog, nextMove);
								System.out.println();
								board.printBoard(board.getChessBoard(), board.getPiecesArray());
								System.out.println();
								this.setDrawRequested(false);
								currentTurnNumber++;
							}else {
								//Should not reach this branch
								System.out.println("Why is move type 4? Neither a king nor a pawn");
							}
							break;
						default:
							//Should not reach this point
							System.out.println("Wrong move type");
							break;
					}
				}else {
					//If searched move returns null, that means it is not a valid move
					System.out.println("Illegal move, try again");
				}
			}else if(moveString.length() == 7 && moveString.charAt(5) == ' ') {
				//System.out.println("Pawn Promotion");
				//This indicates promoting a pawn to a different piece such as knight, bishop, rook
				//And even queen should the player explicitly state it
				int current_x = convertCoordinates(moveString.charAt(0));
				int current_y = convertCoordinates(moveString.charAt(1));
				int new_x = convertCoordinates(moveString.charAt(3));
				int new_y = convertCoordinates(moveString.charAt(4));
				Move inputMove = new Move(current_x, current_y, new_x, new_y, 0);
				if(searchMove(inputMove, validMovesList) != null) {
					//Promotion move is found in the valid moves list
					Move nextMove = searchMove(inputMove, validMovesList);
					if(nextMove.getMove_type() == 2) {
						Piece piece = board.getPieceFromCoordinate(current_x, current_y, piecesArray);
						if(piece instanceof Pawn) {
							Pawn pawn = (Pawn) piece;
							switch(moveString.charAt(6)) {
								case 'N':
									pawn.promoteRegular(chessBoard, piecesArray, nextMove, 'N');
									updateGameLog(gameLog, nextMove);
									System.out.println();
									board.printBoard(board.getChessBoard(), board.getPiecesArray());
									System.out.println();
									this.setDrawRequested(false);
									currentTurnNumber++;
									break;
								case 'B':
									pawn.promoteRegular(chessBoard, piecesArray, nextMove, 'B');
									updateGameLog(gameLog, nextMove);
									System.out.println();
									board.printBoard(board.getChessBoard(), board.getPiecesArray());
									System.out.println();
									this.setDrawRequested(false);
									currentTurnNumber++;
									break;
								case 'R':
									pawn.promoteRegular(chessBoard, piecesArray, nextMove, 'R');
									updateGameLog(gameLog, nextMove);
									System.out.println();
									board.printBoard(board.getChessBoard(), board.getPiecesArray());
									System.out.println();
									this.setDrawRequested(false);
									currentTurnNumber++;
									break;
								case 'Q':
									pawn.promoteRegular(chessBoard, piecesArray, nextMove, 'Q');
									updateGameLog(gameLog, nextMove);
									System.out.println();
									board.printBoard(board.getChessBoard(), board.getPiecesArray());
									System.out.println();
									this.setDrawRequested(false);
									currentTurnNumber++;
									break;
								default:
									//Player entered an invalid promotion
									System.out.println("Illegal move, try again");
									break;
							}
						}else {
							//If piece is not a pawn, it cannot be promoted
							System.out.println("Illegal move, try again");
						}
					}else if(nextMove.getMove_type() == 1) {
						Piece piece = board.getPieceFromCoordinate(current_x, current_y, piecesArray);
						if(piece instanceof Pawn) {
							Pawn pawn = (Pawn) piece;
							switch(moveString.charAt(6)) {
								case 'N':
									pawn.promoteCapture(chessBoard, piecesArray, nextMove, 'N');
									updateGameLog(gameLog, nextMove);
									System.out.println();
									board.printBoard(board.getChessBoard(), board.getPiecesArray());
									System.out.println();
									this.setDrawRequested(true);
									currentTurnNumber++;
									break;
								case 'B':
									pawn.promoteCapture(chessBoard, piecesArray, nextMove, 'B');
									updateGameLog(gameLog, nextMove);
									System.out.println();
									board.printBoard(board.getChessBoard(), board.getPiecesArray());
									System.out.println();
									this.setDrawRequested(true);
									currentTurnNumber++;
									break;
								case 'R':
									pawn.promoteCapture(chessBoard, piecesArray, nextMove, 'R');
									updateGameLog(gameLog, nextMove);
									System.out.println();
									board.printBoard(board.getChessBoard(), board.getPiecesArray());
									System.out.println();
									this.setDrawRequested(true);
									currentTurnNumber++;
									break;
								case 'Q':
									pawn.promoteCapture(chessBoard, piecesArray, nextMove, 'Q');
									updateGameLog(gameLog, nextMove);
									System.out.println();
									board.printBoard(board.getChessBoard(), board.getPiecesArray());
									System.out.println();
									this.setDrawRequested(true);
									currentTurnNumber++;
									break;
								default:
									//Player entered an invalid promotion
									System.out.println("Illegal move, try again");
									break;
							}
						}else {
							//If piece is not a pawn, it cannot be promoted
							System.out.println("Illegal move, try again");
						}
					}else {
						System.out.println("Illegal move, try again");
					}
				}else {
					//If searched move returns null, that means it is not a valid move
					System.out.println("Illegal move, try again");
				}
			}else if(moveString.length() == 11 && moveString.charAt(5) == ' ' && moveString.endsWith("draw?")) {
				//System.out.println("Regular move with draw");
				//Regular move that requests a draw
				int current_x = convertCoordinates(moveString.charAt(0));
				int current_y = convertCoordinates(moveString.charAt(1));
				int new_x = convertCoordinates(moveString.charAt(3));
				int new_y = convertCoordinates(moveString.charAt(4));
				Move inputMove = new Move(current_x, current_y, new_x, new_y, 0);
				if(searchMove(inputMove, validMovesList) != null) {
					//Promotion move is found in valid moves list
					Move nextMove = searchMove(inputMove, validMovesList);
					Piece piece = board.getPieceFromCoordinate(current_x, current_y, piecesArray);
					switch(nextMove.getMove_type()) {
						case 0:
							//Regular move
							//First find the piece you are trying to move
							piece.move(chessBoard, piecesArray, nextMove);
							updateGameLog(gameLog, nextMove);
							System.out.println();
							board.printBoard(board.getChessBoard(), board.getPiecesArray());
							System.out.println();
							this.setDrawRequested(true);
							currentTurnNumber++;
							break;
						case 1:
							//Capture move
							//Check if piece is pawn first
							if(piece instanceof Pawn) {
								//If piece is an instance of pawn, check if move goes to rank 1 or rank 8
								if(nextMove.getNew_y() == 1 || nextMove.getNew_y() == 8) {
									Pawn pawn = (Pawn) piece;
									pawn.promoteCapture(chessBoard, piecesArray, nextMove, 'Q');
									updateGameLog(gameLog, nextMove);
								}else {
									piece.capture(chessBoard, piecesArray, nextMove);
									updateGameLog(gameLog, nextMove);
								}
							}else {
								piece.capture(chessBoard, piecesArray, nextMove);
								updateGameLog(gameLog, nextMove);
							}
							System.out.println();
							board.printBoard(board.getChessBoard(), board.getPiecesArray());
							System.out.println();
							this.setDrawRequested(true);
							currentTurnNumber++;
							break;
						case 2:
							//Castling short for king, promotion for pawn
							if(piece instanceof King) {
								King king = (King) piece;
								king.castle_short(chessBoard, piecesArray, nextMove);
								updateGameLog(gameLog, nextMove);
								System.out.println();
								board.printBoard(board.getChessBoard(), board.getPiecesArray());
								System.out.println();
								this.setDrawRequested(true);
								currentTurnNumber++;
							}else if(piece instanceof Pawn){
								Pawn pawn = (Pawn) piece;
								//Since string length is only 5, no promotion is indicated
								//Therefore it is assumed to be promoted to a queen
								pawn.promoteRegular(chessBoard, piecesArray, nextMove, 'Q');
								updateGameLog(gameLog, nextMove);
								System.out.println();
								board.printBoard(board.getChessBoard(), board.getPiecesArray());
								System.out.println();
								this.setDrawRequested(true);
								currentTurnNumber++;
							}else {
								//Should not reach this branch
								System.out.println("Why is move type 2? It's not a king nor a pawn");
							}
							break;
						case 3:
							//Castling long for king, en passant for pawn
							if(piece instanceof King) {
								King king = (King) piece;
								king.castle_long(chessBoard, piecesArray, nextMove);
								updateGameLog(gameLog, nextMove);
								System.out.println();
								board.printBoard(board.getChessBoard(), board.getPiecesArray());
								System.out.println();
								this.setDrawRequested(true);
								currentTurnNumber++;
							}else if(piece instanceof Pawn) {
								Pawn pawn = (Pawn) piece;
								pawn.en_passant(chessBoard, piecesArray, nextMove);
								updateGameLog(gameLog, nextMove);
								System.out.println();
								board.printBoard(board.getChessBoard(), board.getPiecesArray());
								System.out.println();
								this.setDrawRequested(true);
								currentTurnNumber++;
							}else {
								//Should not reach this branch
								System.out.println("Why is move type 3? Neither a king nor a pawn");
							}
							break;
						default:
							//Should not reach this point
							System.out.println("Wrong move type");
							break;
					}
				}else {
					//Move is not in valid moves list
					System.out.println("Illegal move, try again");
				}
			}else if(moveString.length() == 13 && moveString.charAt(5) == ' ' && moveString.charAt(7) == ' '
					&& moveString.endsWith("draw?")) {
				//System.out.println("Pawn Promotion with draw");
				//Pawn promotion that requests a draw
				int current_x = convertCoordinates(moveString.charAt(0));
				int current_y = convertCoordinates(moveString.charAt(1));
				int new_x = convertCoordinates(moveString.charAt(3));
				int new_y = convertCoordinates(moveString.charAt(4));
				Move inputMove = new Move(current_x, current_y, new_x, new_y, 0);
				if(searchMove(inputMove, validMovesList) != null) {
					//Promotion move is found in the valid moves list
					Move nextMove = searchMove(inputMove, validMovesList);
					if(nextMove.getMove_type() == 2) {
						Piece piece = board.getPieceFromCoordinate(current_x, current_y, piecesArray);
						if(piece instanceof Pawn) {
							Pawn pawn = (Pawn) piece;
							switch(moveString.charAt(6)) {
								case 'N':
									pawn.promoteRegular(chessBoard, piecesArray, nextMove, 'N');
									updateGameLog(gameLog, nextMove);
									System.out.println();
									board.printBoard(board.getChessBoard(), board.getPiecesArray());
									System.out.println();
									this.setDrawRequested(true);
									currentTurnNumber++;
									break;
								case 'B':
									pawn.promoteRegular(chessBoard, piecesArray, nextMove, 'B');
									updateGameLog(gameLog, nextMove);
									System.out.println();
									board.printBoard(board.getChessBoard(), board.getPiecesArray());
									System.out.println();
									this.setDrawRequested(true);
									currentTurnNumber++;
									break;
								case 'R':
									pawn.promoteRegular(chessBoard, piecesArray, nextMove, 'R');
									updateGameLog(gameLog, nextMove);
									System.out.println();
									board.printBoard(board.getChessBoard(), board.getPiecesArray());
									System.out.println();
									this.setDrawRequested(true);
									currentTurnNumber++;
									break;
								case 'Q':
									pawn.promoteRegular(chessBoard, piecesArray, nextMove, 'Q');
									updateGameLog(gameLog, nextMove);
									System.out.println();
									board.printBoard(board.getChessBoard(), board.getPiecesArray());
									System.out.println();
									this.setDrawRequested(true);
									currentTurnNumber++;
									break;
								default:
									//Player entered an invalid promotion
									System.out.println("Illegal move, try again");
									break;
							}
						}else {
							//If piece is not a pawn, it cannot be promoted
							System.out.println("Illegal move, try again");
						}
					}else if(nextMove.getMove_type() == 1) {
						Piece piece = board.getPieceFromCoordinate(current_x, current_y, piecesArray);
						if(piece instanceof Pawn) {
							Pawn pawn = (Pawn) piece;
							switch(moveString.charAt(6)) {
								case 'N':
									pawn.promoteCapture(chessBoard, piecesArray, nextMove, 'N');
									updateGameLog(gameLog, nextMove);
									System.out.println();
									board.printBoard(board.getChessBoard(), board.getPiecesArray());
									System.out.println();
									this.setDrawRequested(true);
									currentTurnNumber++;
									break;
								case 'B':
									pawn.promoteCapture(chessBoard, piecesArray, nextMove, 'B');
									updateGameLog(gameLog, nextMove);
									System.out.println();
									board.printBoard(board.getChessBoard(), board.getPiecesArray());
									System.out.println();
									this.setDrawRequested(true);
									currentTurnNumber++;
									break;
								case 'R':
									pawn.promoteCapture(chessBoard, piecesArray, nextMove, 'R');
									updateGameLog(gameLog, nextMove);
									System.out.println();
									board.printBoard(board.getChessBoard(), board.getPiecesArray());
									System.out.println();
									this.setDrawRequested(true);
									currentTurnNumber++;
									break;
								case 'Q':
									pawn.promoteCapture(chessBoard, piecesArray, nextMove, 'Q');
									updateGameLog(gameLog, nextMove);
									System.out.println();
									board.printBoard(board.getChessBoard(), board.getPiecesArray());
									System.out.println();
									this.setDrawRequested(true);
									currentTurnNumber++;
									break;
								default:
									//Player entered an invalid promotion
									System.out.println("Illegal move, try again");
									break;
							}
						}else {
							//If piece is not a pawn, it cannot be promoted
							System.out.println("Illegal move, try again");
						}
					}else {
						System.out.println("Illegal move, try again");
					}
				}else {
					//If searched move returns null, that means it is not a valid move
					System.out.println("Illegal move, try again");
				}
			}else {
				//Everything else				
				System.out.println("Illegal move, try again");
			}
		}else {
			System.out.println("Everything else");
			System.out.println("Illegal move, try again");
		}
	}
	/** Control method to start a game and keep it running
	 */
	public void playGame() {
		
		GameState gameState = GameState.NORMAL;
		
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		
		//Create a game log to keep track of the moves throughout the game
		ArrayList<Turn> gameLog = new ArrayList<Turn>();
		
		//Create new instance of board
		Board board = new Board();
		
		//Create an 8x8 Tile array and set that as the chess board
		Tile[][] chessBoard = new Tile[8][8];
		for(int i=0; i<8; i++) {
			for(int j=0; j<8; j++) {
				chessBoard[i][j] = new Tile();
			}
		}
		board.setChessBoard(chessBoard);
		
		//Create array to keep track of pieces and locations
		Piece[] piecesArray = new Piece[32];
		board.setPiecesArray(piecesArray);
		
		board.initializeGame(board.getChessBoard(), board.getPiecesArray());
		board.printBoard(board.getChessBoard(), board.getPiecesArray());
				
		System.out.println();
		
		while(gameState == GameState.NORMAL) {
			//System.out.println("Current black king: " +  +piecesArray[31].getY_coordinate());
			//System.out.println("Current white king: " + piecesArray[15].getX_coordinate() +piecesArray[15].getY_coordinate()); 
			try {
				
				//If turn number is odd, white goes, if turn number is even, black goes
				if(currentTurnNumber%2 == 1) {
					//White's move
					
					//First pull up a list of all valid moves white has
					ArrayList<Move> validMovesList = new ArrayList<Move>();
					for(int i=0; i<16; i++) {
						if(piecesArray[i] == null) {
							continue;
						}
						ArrayList<Move> unfilteredMovesList = piecesArray[i].getPossibleMoves(chessBoard, board, piecesArray, gameLog);
						ArrayList<Move> validMovesForPiece = piecesArray[i].getValidMoves(chessBoard, board, piecesArray, unfilteredMovesList, gameLog);
						validMovesList.addAll(validMovesForPiece);
					}
					Game.black_king_x = piecesArray[31].getX_coordinate();
					Game.black_king_y = piecesArray[31].getY_coordinate();
					Game.white_king_x = piecesArray[15].getX_coordinate();
					Game.white_king_y = piecesArray[15].getY_coordinate();
					
					//If there are no valid moves, check if white is in check. If so
					//Black wins by checkmate. If not, it's a stalemate
					int check = board.inCheck(chessBoard, piecesArray, gameLog);
					if(validMovesList.isEmpty()) {
						if(check == 1) {
							//If white is in check, and validMovesList is empty
							//White is in checkmate, black wins
							gameState = GameState.BLACK_WINS_C;
							break;
						}else {
							gameState = GameState.STALEMATE;
							break;
						}
					}
					if(check == 1) {
						System.out.println("Check\n");
					}
					
					System.out.print("White's move: ");
					String moveString = br.readLine();
					
					if(moveString.equals("resign")) {
						//System.out.println("Black wins");
						currentTurnNumber++;
						gameState = GameState.BLACK_WINS_R;
						break;
					}else if(moveString.equals("draw")) {
						//First check if a draw is currently being requested
						if(this.isDrawRequested() == false) {
							//If draw is not currently being requested, it's an illegal move
							System.out.println("Illegal move, try again");
						}else {
							//If a draw is currently being requested, game ends in a draw
							currentTurnNumber++;
							gameState = GameState.DRAW;
							break;
						}
					}else {
						parseMove(moveString, validMovesList, board, drawRequested, gameLog);
					}
				}else {
					//Black's move
					
					//First pull up a list of all valid moves black has
					ArrayList<Move> validMovesList = new ArrayList<Move>();
					for(int i=16; i<32; i++) {
						if(piecesArray[i] == null) {
							continue;
						}
						ArrayList<Move> unfilteredMovesList = piecesArray[i].getPossibleMoves(chessBoard, board, piecesArray, gameLog);
						ArrayList<Move> validMovesForPiece = piecesArray[i].getValidMoves(chessBoard, board, piecesArray, unfilteredMovesList, gameLog);
						validMovesList.addAll(validMovesForPiece);
					}
					
					//If there are no valid moves, check if black is in check. If so
					//White wins by checkmate. If not, it's a stalemate
					int check = board.inCheck(chessBoard, piecesArray, gameLog);
					if(validMovesList.isEmpty()) {
						if(check == 2) {
							//If white is in check, and validMovesList is empty
							//White is in checkmate, black wins
							gameState = GameState.WHITE_WINS_C;
							break;
						}else {
							gameState = GameState.STALEMATE;
							break;
						}
					}
					if(check == 2) {
						System.out.println("Check\n");
					}
					
					System.out.print("Black's move: ");
					String moveString = br.readLine();
					
					if(moveString.equals("resign")) {
						//System.out.println("Black wins");
						currentTurnNumber++;
						gameState = GameState.WHITE_WINS_R;
					}else if(moveString.equals("draw")) {
						//First check if a draw is currently being requested
						if(this.isDrawRequested() == false) {
							//If draw is not currently being requested, it's an illegal move
							System.out.println("Illegal move, try again");
						}else {
							//If a draw is currently being requested, game ends in a draw
							currentTurnNumber++;
							gameState = GameState.DRAW;
						}
					}else {
						parseMove(moveString, validMovesList, board, drawRequested, gameLog);
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		switch(gameState) {
			case WHITE_WINS_C:
				System.out.println("Checkmate");
				System.out.println("White wins");
				break;
			case BLACK_WINS_C:
				System.out.println("Checkmate");
				System.out.println("Black wins");
				break;
			case STALEMATE:
				System.out.println("Stalemate");
				System.out.println("draw");
				break;
			case DRAW:
				System.out.println("draw");
				break;
			case WHITE_WINS_R:
				System.out.println("White wins");
				break;
			case BLACK_WINS_R:
				System.out.println("Black wins");
				break;
			default:
				//Should not reach here
				System.out.println("Game state error");
		}
	}

	public static int getCurrentTurnNumber() {
		return currentTurnNumber;
	}

	public static void setCurrentTurnNumber(int currentTurnNumber) {
		Game.currentTurnNumber = currentTurnNumber;
	}

}

//e2 e4
//d7 d5
