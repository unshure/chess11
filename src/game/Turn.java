package game;

import pieces.Move;

/** Stores information for a move and a turn number
 * 
 * @author Nick Clegg
 * @author Kevin Wu
 * @version 1.0
 *
 */
public class Turn {
	
	private Move move;
	private int turn_number;
	
	public Turn(Move move, int turn_number) {
		this.move = move;
		this.turn_number = turn_number;
	}

	public Move getMove() {
		return move;
	}

	public void setMove(Move move) {
		this.move = move;
	}

	public int getTurn_number() {
		return turn_number;
	}

	public void setTurn_number(int turn_number) {
		this.turn_number = turn_number;
	}

}
