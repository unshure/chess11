package pieces;

import java.util.ArrayList;

import game.Board;
import game.Tile;
import game.Turn;

/** Gives outline for all chess pieces
 * 
 * @author Nick Clegg
 * @author Kevin Wu
 * @version 1.0
 *
 */
public abstract class Piece {
	
	//Fields of the pieces
	
	/**For color, 0 stands for white, 1 stands for black
	 */
	private int color;
	private int piece_id;
	private int x_coordinate;
	private int y_coordinate;
	
	/** Moves piece to specified place on board
	 * @param chessBoard The current board
	 * @param piecesArray array of all pieces and their location
	 * @param move Stores information needed for a move event
	 */
	public abstract void move(Tile[][] chessBoard, Piece[] piecesArray, Move move);
	
	/** Pieces can capture other pieces
	 * 
	 * @param chessBoard The current board
	 * @param piecesArray array of all pieces and their location
	 * @param move Stores information needed for a move event
	 */
	public abstract void capture(Tile[][] chessBoard, Piece[] piecesArray, Move move);
	
	/** Returns a list of possible moves
	 * 
	 * @param grid 2D array of tiles
	 * @param board Current Board
	 * @param piecesArray array of all pieces and their location
	 * @param gameLog Log of all previous moves
	 * @return List of possible moves
	 */
	public abstract ArrayList<Move> getPossibleMoves(Tile[][] grid, Board board, Piece[] piecesArray, ArrayList<Turn> gameLog);
	
	/** Takes list of possible moves, removes invalid ones (like ones that result in check), and returns
	 *  the filtered list
	 * @param grid 2D array of tiles
	 * @param board Current Board
	 * @param piecesArray array of all pieces and their location
	 * @param unfilteredList List from getPossibleMoves
	 * @param gameLog List of all previous moves
	 * @return List of Valid Moves
	 */
	public abstract ArrayList<Move> getValidMoves(Tile[][] grid, Board board, Piece[] piecesArray, ArrayList<Move> unfilteredList, ArrayList<Turn> gameLog);

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public int getPiece_id() {
		return piece_id;
	}

	public void setPiece_id(int piece_id) {
		this.piece_id = piece_id;
	}

	public int getX_coordinate() {
		return x_coordinate;
	}

	public void setX_coordinate(int x_coordinate) {
		this.x_coordinate = x_coordinate;
	}

	public int getY_coordinate() {
		return y_coordinate;
	}

	public void setY_coordinate(int y_coordinate) {
		this.y_coordinate = y_coordinate;
	}

}
