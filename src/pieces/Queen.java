package pieces;

import java.util.ArrayList;

import game.Board;
import game.Tile;
import game.Turn;

//Queens can move orthogonally or diagonally
/** Represents a Queen Piece
 * @author	Nick Clegg
 * @author	Kevin Wu
 * @version	1.0
 */
public class Queen extends Piece {

	@Override
	public void move(Tile[][] chessBoard, Piece[] piecesArray, Move move) {
		// TODO Auto-generated method stub
		int current_x = move.getCurrent_x();
		int current_y = move.getCurrent_y();
		
		for(int i=0; i<piecesArray.length; i++) {
			if(piecesArray[i] == null) {
				continue;
			}else if(piecesArray[i].getX_coordinate() == current_x && piecesArray[i].getY_coordinate() == current_y) {
				int new_x = move.getNew_x();
				int new_y = move.getNew_y();
				piecesArray[i].setX_coordinate(new_x);
				piecesArray[i].setY_coordinate(new_y);
				chessBoard[current_y - 1][current_x - 1].setOccupied(false);
				chessBoard[new_y - 1][new_x - 1].setOccupied(true);
				break;
			}
		}
	}

	@Override
	public void capture(Tile[][] chessBoard, Piece[] piecesArray, Move move) {
		// TODO Auto-generated method stub
		
		int new_x = move.getNew_x();
		int new_y = move.getNew_y();
		
		for(int i=0; i<piecesArray.length; i++) {
			if(piecesArray[i] == null) {
				continue;
			}else if(piecesArray[i].getX_coordinate() == new_x && piecesArray[i].getY_coordinate() == new_y) {
				piecesArray[i] = null;
				break;
			}
		}
		
		int current_x = move.getCurrent_x();
		int current_y = move.getCurrent_y();
		
		for(int i=0; i<piecesArray.length; i++) {
			if(piecesArray[i] == null) {
				continue;
			}else if(piecesArray[i].getX_coordinate() == current_x && piecesArray[i].getY_coordinate() == current_y) {
				piecesArray[i].setX_coordinate(new_x);
				piecesArray[i].setY_coordinate(new_y);
				chessBoard[current_y - 1][current_x - 1].setOccupied(false);
				break;
			}
		}
	}
	/** Helper method to check if piece is in board
	 * 
	 * @param x_coordinate X coordinate to check
	 * @param y_coordinate Y coordinate to check
	 * @return True if in bounds, False if not in bounds
	 */
	public boolean inBoard(int x_coordinate, int y_coordinate) {
		boolean inBounds = true;
		if(x_coordinate < 1 || x_coordinate > 8 || y_coordinate < 1 || y_coordinate > 8) {
			inBounds = false;
		}
		return inBounds;
	}

	@Override
	public ArrayList<Move> getPossibleMoves(Tile[][] grid, Board board, Piece[] piecesArray, ArrayList<Turn> gameLog) {
		// TODO Auto-generated method stub
		ArrayList<Move> unobstructedList = new ArrayList<Move>();
		
		int current_x = this.getX_coordinate();
		int current_y = this.getY_coordinate();
		
		int new_x = current_x;
		int new_y = current_y;
		
		//Checks all valid moves to the right
		do {
			new_x++;
			
			if(!inBoard(new_x, new_y)) {
				break;
			}
			
			if(grid[new_y - 1][new_x - 1].isOccupied()) {
				//If the square to the top right is occupied
				//Check if it is a friendly piece first
				//If so, don't generate possible move
				//If not, generate a capture move
				Piece obstructingPiece = board.getPieceFromCoordinate(new_x, new_y, piecesArray);
				if(obstructingPiece.getColor() == this.getColor()) {
					break;
				}else {
					Move newMove = new Move(current_x, current_y, new_x, new_y, 1);
					unobstructedList.add(newMove);
					break;
				}
			}else {
				Move newMove = new Move(current_x, current_y, new_x, new_y, 0);
				unobstructedList.add(newMove);
			}
		}while(true);
		
		new_x = current_x;
		new_y = current_y;
		
		//Checks all valid moves to the left
		do {
			new_x--;
			
			if(!inBoard(new_x, new_y)) {
				break;
			}
			
			if(grid[new_y - 1][new_x - 1].isOccupied()) {
				//If the square to the top right is occupied
				//Check if it is a friendly piece first
				//If so, don't generate possible move
				//If not, generate a capture move
				Piece obstructingPiece = board.getPieceFromCoordinate(new_x, new_y, piecesArray);
				if(obstructingPiece.getColor() == this.getColor()) {
					break;
				}else {
					Move newMove = new Move(current_x, current_y, new_x, new_y, 1);
					unobstructedList.add(newMove);
					break;
				}
			}else {
				Move newMove = new Move(current_x, current_y, new_x, new_y, 0);
				unobstructedList.add(newMove);
			}
		}while(true);
		
		new_x = current_x;
		new_y = current_y;
		
		//Checks all valid moves to the top
		do {
			new_y++;
			
			if(!inBoard(new_x, new_y)) {
				break;
			}
			
			if(grid[new_y - 1][new_x - 1].isOccupied()) {
				//If the square to the top right is occupied
				//Check if it is a friendly piece first
				//If so, don't generate possible move
				//If not, generate a capture move
				Piece obstructingPiece = board.getPieceFromCoordinate(new_x, new_y, piecesArray);
				if(obstructingPiece.getColor() == this.getColor()) {
					break;
				}else {
					Move newMove = new Move(current_x, current_y, new_x, new_y, 1);
					unobstructedList.add(newMove);
					break;
				}
			}else {
				Move newMove = new Move(current_x, current_y, new_x, new_y, 0);
				unobstructedList.add(newMove);
			}
		}while(true);
		
		new_x = current_x;
		new_y = current_y;
		
		//Checks all valid moves to the bottom
		do {
			new_y--;
			
			if(!inBoard(new_x, new_y)) {
				break;
			}
			
			if(grid[new_y - 1][new_x - 1].isOccupied()) {
				//If the square to the top right is occupied
				//Check if it is a friendly piece first
				//If so, don't generate possible move
				//If not, generate a capture move
				Piece obstructingPiece = board.getPieceFromCoordinate(new_x, new_y, piecesArray);
				if(obstructingPiece.getColor() == this.getColor()) {
					break;
				}else {
					Move newMove = new Move(current_x, current_y, new_x, new_y, 1);
					unobstructedList.add(newMove);
					break;
				}
			}else {
				Move newMove = new Move(current_x, current_y, new_x, new_y, 0);
				unobstructedList.add(newMove);
			}
		}while(true);
		
		new_x = current_x;
		new_y = current_y;
		
		//Searches all valid moves to the top right
		do {
			new_x++;
			new_y++;
			
			if(!inBoard(new_x, new_y)) {
				break;
			}
			
			if(grid[new_y - 1][new_x - 1].isOccupied()) {
				//If the square to the top right is occupied
				//Check if it is a friendly piece first
				//If so, don't generate possible move
				//If not, generate a capture move
				Piece obstructingPiece = board.getPieceFromCoordinate(new_x, new_y, piecesArray);
				if(obstructingPiece.getColor() == this.getColor()) {
					break;
				}else {
					Move newMove = new Move(current_x, current_y, new_x, new_y, 1);
					unobstructedList.add(newMove);
					break;
				}
			}else {
				Move newMove = new Move(current_x, current_y, new_x, new_y, 0);
				unobstructedList.add(newMove);
			}
		}while(true);
		
		new_x = current_x;
		new_y = current_y;
		
		//Searches all valid moves to the bottom right
		do {
			new_x++;
			new_y--;
			
			if(!inBoard(new_x, new_y)) {
				break;
			}
			
			if(grid[new_y - 1][new_x - 1].isOccupied()) {
				//If the square to the top right is occupied
				//Check if it is a friendly piece first
				//If so, don't generate possible move
				//If not, generate a capture move
				Piece obstructingPiece = board.getPieceFromCoordinate(new_x, new_y, piecesArray);
				if(obstructingPiece.getColor() == this.getColor()) {
					break;
				}else {
					Move newMove = new Move(current_x, current_y, new_x, new_y, 1);
					unobstructedList.add(newMove);
					break;
				}
			}else {
				Move newMove = new Move(current_x, current_y, new_x, new_y, 0);
				unobstructedList.add(newMove);
			}
		}while(true);
		
		new_x = current_x;
		new_y = current_y;
		
		//Searches all valid moves to the bottom right
		do {
			new_x--;
			new_y++;
			
			if(!inBoard(new_x, new_y)) {
				break;
			}
			
			if(grid[new_y - 1][new_x - 1].isOccupied()) {
				//If the square to the top right is occupied
				//Check if it is a friendly piece first
				//If so, don't generate possible move
				//If not, generate a capture move
				Piece obstructingPiece = board.getPieceFromCoordinate(new_x, new_y, piecesArray);
				if(obstructingPiece.getColor() == this.getColor()) {
					break;
				}else {
					Move newMove = new Move(current_x, current_y, new_x, new_y, 1);
					unobstructedList.add(newMove);
					break;
				}
			}else {
				Move newMove = new Move(current_x, current_y, new_x, new_y, 0);
				unobstructedList.add(newMove);
			}
		}while(true);
		
		new_x = current_x;
		new_y = current_y;
		
		//Searches all valid moves to the bottom left
		do {
			new_x--;
			new_y--;
			
			if(!inBoard(new_x, new_y)) {
				break;
			}
			
			if(grid[new_y - 1][new_x - 1].isOccupied()) {
				//If the square to the top right is occupied
				//Check if it is a friendly piece first
				//If so, don't generate possible move
				//If not, generate a capture move
				Piece obstructingPiece = board.getPieceFromCoordinate(new_x, new_y, piecesArray);
				if(obstructingPiece.getColor() == this.getColor()) {
					break;
				}else {
					Move newMove = new Move(current_x, current_y, new_x, new_y, 1);
					unobstructedList.add(newMove);
					break;
				}
			}else {
				Move newMove = new Move(current_x, current_y, new_x, new_y, 0);
				unobstructedList.add(newMove);
			}
		}while(true);
		
		return unobstructedList;
	}

	@Override
	public ArrayList<Move> getValidMoves(Tile[][] grid, Board board, Piece[] piecesArray,
			ArrayList<Move> unfilteredList, ArrayList<Turn> gameLog) {
		// TODO Auto-generated method stub
		ArrayList<Move> validMovesList = new ArrayList<Move>();
		for(int i=0; i<unfilteredList.size(); i++) {
			Move move = unfilteredList.get(i);
			int check = board.resultInCheck(grid, piecesArray, move, gameLog);
			if(this.getColor() == 0) {
				//If you are playing as white, you can't make a move such that the white king will be checked
				if(check != 1) {
					validMovesList.add(move);
				}else {
					continue;
				}
			}else if(this.getColor() == 1) {
				//If you are playing as black, you can't make a move such that the black king will be checked
				if(check != 2) {
					validMovesList.add(move);
				}else {
					continue;
				}
			}
		}
		return validMovesList;
	}
}
