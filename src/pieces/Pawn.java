package pieces;

import java.util.ArrayList;
import game.Board;
import game.Tile;
import game.Turn;


/** Represents a Pawn Piece
 * @author	Nick Clegg
 * @author	Kevin Wu
 * @version	1.0
 */
public class Pawn extends Piece {
	
	//Special_1 or move_type = 2 is defined as promotion
	//Special_2 or move_type = 3 is defined as en passant
	/** Tells if this piece has moved yet
	 */
	boolean moved = false;

	@Override
	public void move(Tile[][] chessBoard, Piece[] piecesArray, Move move) {
		// TODO Auto-generated method stub
		int current_x = move.getCurrent_x();
		int current_y = move.getCurrent_y();
		
		for(int i=0; i<piecesArray.length; i++) {
			if(piecesArray[i] == null) {
				continue;
			}else if(piecesArray[i].getX_coordinate() == current_x && piecesArray[i].getY_coordinate() == current_y) {
				int new_x = move.getNew_x();
				int new_y = move.getNew_y();
				piecesArray[i].setX_coordinate(new_x);
				piecesArray[i].setY_coordinate(new_y);
				chessBoard[current_y - 1][current_x - 1].setOccupied(false);
				chessBoard[new_y - 1][new_x - 1].setOccupied(true);
				break;
			}
		}
		this.setMoved(true);
	}

	public boolean isMoved() {
		return moved;
	}

	public void setMoved(boolean moved) {
		this.moved = moved;
	}

	@Override
	public void capture(Tile[][] chessBoard, Piece[] piecesArray, Move move) {
		// TODO Auto-generated method stub
		
		int new_x = move.getNew_x();
		int new_y = move.getNew_y();
		
		for(int i=0; i<piecesArray.length; i++) {
			if(piecesArray[i] == null) {
				continue;
			}else if(piecesArray[i].getX_coordinate() == new_x && piecesArray[i].getY_coordinate() == new_y) {
				piecesArray[i] = null;
				break;
			}
		}
		
		int current_x = move.getCurrent_x();
		int current_y = move.getCurrent_y();
		
		for(int i=0; i<piecesArray.length; i++) {
			if(piecesArray[i] == null) {
				continue;
			}else if(piecesArray[i].getX_coordinate() == current_x && piecesArray[i].getY_coordinate() == current_y) {
				piecesArray[i].setX_coordinate(new_x);
				piecesArray[i].setY_coordinate(new_y);
				chessBoard[current_y - 1][current_x - 1].setOccupied(false);
				break;
			}
		}
		this.setMoved(true);
	}
	/** Promotes a pawn if it reaches the opposite side of the board
	 *
	 * @param chessBoard Current Board
	 * @param piecesArray array of all pieces and their location
	 * @param move Current move
	 * @param promotion Character to represent the promotion
	 */
	public void promoteRegular(Tile[][] chessBoard, Piece[] piecesArray, Move move, char promotion) {
		
		int current_x = move.getCurrent_x();
		int current_y = move.getCurrent_y();
		
		for(int i=0; i<piecesArray.length; i++) {
			if(piecesArray[i] == null) {
				continue;
			}else if(piecesArray[i].getX_coordinate() == current_x && piecesArray[i].getY_coordinate() == current_y) {
				int new_x = move.getNew_x();
				int new_y = move.getNew_y();
				piecesArray[i].setX_coordinate(new_x);
				piecesArray[i].setY_coordinate(new_y);
				chessBoard[current_y - 1][current_x - 1].setOccupied(false);
				chessBoard[new_y - 1][new_x - 1].setOccupied(true);
				
				switch(promotion) {
					case 'N':
						Knight newKnight = new Knight();
						newKnight.setColor(this.getColor());
						newKnight.setX_coordinate(move.getNew_x());
						newKnight.setY_coordinate(move.getNew_y());
						piecesArray[i] = newKnight;
						break;
					case 'B':
						Bishop newBishop = new Bishop();
						newBishop.setColor(this.getColor());
						newBishop.setX_coordinate(move.getNew_x());
						newBishop.setY_coordinate(move.getNew_y());
						piecesArray[i] = newBishop;
						break;
					case 'R':
						Rook newRook = new Rook();
						newRook.setColor(this.getColor());
						newRook.setX_coordinate(move.getNew_x());
						newRook.setY_coordinate(move.getNew_y());
						piecesArray[i] = newRook;
						break;
					case 'Q':
						Queen newQueen = new Queen();
						newQueen.setColor(this.getColor());
						newQueen.setX_coordinate(move.getNew_x());
						newQueen.setY_coordinate(move.getNew_y());
						piecesArray[i] = newQueen;
						break;
					default:
						//Invalid promotion
						System.out.println("Illegal move, try again");
						break;
				}
				break;
			}
		}
	}
	/** Capture a Piece and Promotes a pawn if it reaches the opposite side of the board
	 *
	 * @param chessBoard Current Board
	 * @param piecesArray array of all pieces and their location
	 * @param move Current move
	 * @param promotion Character to represent the promotion
	 */
	public void promoteCapture(Tile[][] chessBoard, Piece[] piecesArray, Move move, char promotion) {
		
		int current_x = move.getCurrent_x();
		int current_y = move.getCurrent_y();
		
		//this.capture(chessBoard, piecesArray, move);
		
		for(int i=0; i<piecesArray.length; i++) {
			if(piecesArray[i] == null) {
				continue;
			}else if(piecesArray[i].getX_coordinate() == current_x && piecesArray[i].getY_coordinate() == current_y) {
				int new_x = move.getNew_x();
				int new_y = move.getNew_y();
				for(int j=0; j<piecesArray.length; j++) 	{
					if(piecesArray[j] == null) {
						continue;
					}else if(piecesArray[j].getX_coordinate() == new_x && piecesArray[j].getY_coordinate() == new_y) {
						piecesArray[j] = null;
						break;
					}
				}
				piecesArray[i].setX_coordinate(new_x);
				piecesArray[i].setY_coordinate(new_y);
				chessBoard[current_y - 1][current_x - 1].setOccupied(false);
				chessBoard[new_y - 1][new_x - 1].setOccupied(true);
				
				switch(promotion) {
					case 'N':
						Knight newKnight = new Knight();
						newKnight.setColor(this.getColor());
						newKnight.setX_coordinate(move.getNew_x());
						newKnight.setY_coordinate(move.getNew_y());
						piecesArray[i] = newKnight;
						break;
					case 'B':
						Bishop newBishop = new Bishop();
						newBishop.setColor(this.getColor());
						newBishop.setX_coordinate(move.getNew_x());
						newBishop.setY_coordinate(move.getNew_y());
						piecesArray[i] = newBishop;
						break;
					case 'R':
						Rook newRook = new Rook();
						newRook.setColor(this.getColor());
						newRook.setX_coordinate(move.getNew_x());
						newRook.setY_coordinate(move.getNew_y());
						piecesArray[i] = newRook;
						break;
					case 'Q':
						Queen newQueen = new Queen();
						newQueen.setColor(this.getColor());
						newQueen.setX_coordinate(move.getNew_x());
						newQueen.setY_coordinate(move.getNew_y());
						piecesArray[i] = newQueen;
						break;
					default:
						//Invalid promotion
						System.out.println("Illegal move, try again");
						break;
				}
				break;
			}
		}
	}
	/** Pawn En Passant Move
	 * 
	 * @param chessBoard Current Board
	 * @param piecesArray array of all pieces and their location
	 * @param move Current move
	 */
	public void en_passant(Tile[][] chessBoard, Piece[] piecesArray, Move move) {
		int enemy_pawn_x = move.getNew_x();
		int enemy_pawn_y = move.getCurrent_y();
		
		for(int i=0; i<piecesArray.length; i++) {
			//Find enemy pawn to capture and remove from the array
			if(piecesArray[i] == null) {
				continue;
			}else if(piecesArray[i].getX_coordinate() == enemy_pawn_x && piecesArray[i].getY_coordinate() == enemy_pawn_y) {
				chessBoard[enemy_pawn_y - 1][enemy_pawn_x - 1].setOccupied(false);
				piecesArray[i] = null;
				break;
			}
		}
		
		for(int i=0; i<piecesArray.length; i++) {
			if(piecesArray[i] == null) {
				continue;
			}else if(piecesArray[i].getX_coordinate() == move.getCurrent_x() && piecesArray[i].getY_coordinate() == move.getCurrent_y()) {
				chessBoard[move.getCurrent_y() - 1][move.getCurrent_x() - 1].setOccupied(false);
				chessBoard[move.getNew_y() - 1][move.getNew_x() - 1].setOccupied(true);
				piecesArray[i].setX_coordinate(move.getNew_x());
				piecesArray[i].setY_coordinate(move.getNew_y());
				break;
			}
		}
	}
	
	/** Checks if coordinate is in bounds of the board
	 * 
	 * @param x_coordinate X coordinate to check
	 * @param y_coordinate Y coordinate to check
	 * @return True if in bounds, False if not in bounds
	 */
	public boolean inBoard(int x_coordinate, int y_coordinate) {
		boolean inBounds = true;
		if(x_coordinate < 1 || x_coordinate > 8 || y_coordinate < 1 || y_coordinate > 8) {
			inBounds = false;
		}
		return inBounds;
	}

	@Override
	public ArrayList<Move> getPossibleMoves(Tile[][] grid, Board board, Piece[] piecesArray, ArrayList<Turn> gameLog) {
		// TODO Auto-generated method stub
		ArrayList<Move> unobstructedList = new ArrayList<Move>();
		
		int current_x = this.getX_coordinate();
		int current_y = this.getY_coordinate();
		
		int new_x = current_x;
		int new_y = current_y;
		
		//If pawn is white, use code from this branch
		if(this.getColor() == 0) {
			//If the pawn has not been moved, you can move two spaces on the first move
			if(this.isMoved() == false && inBoard(current_x, current_y + 2)) {
				if(!grid[new_y + 1][new_x - 1].isOccupied() && !grid[new_y][new_x - 1].isOccupied()) {
					new_y = current_y + 2;
					Move newMove = new Move(current_x, current_y, new_x, new_y, 0);
					unobstructedList.add(newMove);
					new_x = current_x;
					new_y = current_y;
				}
			}
			
			//Pawn can move forward only if grid directly in front is not occupied
			if(!inBoard(current_x, current_y + 1)) {
				//do nothing
			}else if(!grid[current_y][current_x - 1].isOccupied()) {
				new_y = current_y + 1;
				//If the pawn reaches the eight rank, it has to be promoted
				if(new_y == 8) {
					Move newMove = new Move(current_x, current_y, new_x, new_y, 2);
					unobstructedList.add(newMove);
				}else {
					Move newMove = new Move(current_x, current_y, new_x, new_y, 0);
					unobstructedList.add(newMove);
				}
				new_x = current_x;
				new_y = current_y;
			}
			
			//Pawn can only move one space diagonally forward to capture an enemy piece
			if(!inBoard(new_x + 1, new_y + 1)) {
				//do nothing
			}else if(grid[new_y][new_x].isOccupied()) {
				Piece obstructingPiece = board.getPieceFromCoordinate(new_x + 1, new_y + 1, piecesArray);
				if(obstructingPiece.getColor() != this.getColor()) {
					Move newMove = new Move(current_x, current_y, new_x + 1, new_y + 1, 1);
					unobstructedList.add(newMove);
				}
				new_x = current_x;
				new_y = current_y;
			}
			
			if(!inBoard(new_x - 1, new_y + 1)) {
				//do nothing
			}else if(grid[new_y][new_x - 2].isOccupied()) {
				Piece obstructingPiece = board.getPieceFromCoordinate(new_x - 1, new_y + 1, piecesArray);
				if(obstructingPiece.getColor() != this.getColor()) {
					Move newMove = new Move(current_x, current_y, new_x - 1, new_y + 1, 1);
					unobstructedList.add(newMove);
				}
				new_x = current_x;
				new_y = current_y;
			}
			
			//Checks if a white pawn could en passant
			if(current_y == 5) {
				//En passant for white is only possible if the pawn is on the 5th rank
				//Search to the immediate left and right of the pawn so see if the tiles are occupied
				if(inBoard(current_x - 1, current_y)) {
					if(grid[current_y - 1][current_x - 2].isOccupied()) {
						//If the tile to the left is occupied, check if the piece is an enemy pawn
						Piece piece = board.getPieceFromCoordinate(current_x - 1, current_y, piecesArray);
						if(piece.getColor() != this.getColor() && piece instanceof Pawn) {
							//Check if the move was made immediately on the turn immediately before the current turn
							Turn last_turn = gameLog.get(gameLog.size() - 1);
							Move last_move = last_turn.getMove();
							if(last_move.getNew_x() == last_move.getCurrent_x() && last_move.getNew_y() == last_move.getCurrent_y() - 2) {
								Move newMove = new Move(current_x, current_y, current_x - 1, current_y + 1, 3);
								unobstructedList.add(newMove);
							}
						}
					}
				}
				
				if(inBoard(current_x + 1, current_y)) {
					if(grid[current_y - 1][current_x].isOccupied()) {
						//If the tile to the right is occupied, check if the piece is an enemy pawn
						Piece piece = board.getPieceFromCoordinate(current_x + 1, current_y, piecesArray);
						if(piece.getColor() != this.getColor() && piece instanceof Pawn) {
							//Check if the move was made immediately on the turn immediately before the current turn
							Turn last_turn = gameLog.get(gameLog.size() - 1);
							Move last_move = last_turn.getMove();
							if(last_move.getNew_x() == last_move.getCurrent_x() && last_move.getNew_y() == last_move.getCurrent_y() - 2) {
								Move newMove = new Move(current_x, current_y, current_x + 1, current_y + 1, 3);
								unobstructedList.add(newMove);
							}
						}
					}
				}
			}
			
		}else {
			//If pawn is black, use code from this branch
			//If the pawn has not been moved, you can move two spaces on the first move
			if(this.isMoved() == false && inBoard(current_x, current_y - 2)) {
				if(!grid[new_y - 3][new_x - 1].isOccupied() && !grid[new_y - 2][new_x - 1].isOccupied()) {
					new_y = current_y - 2;
					Move newMove = new Move(current_x, current_y, new_x, new_y, 0);
					unobstructedList.add(newMove);
					new_x = current_x;
					new_y = current_y;
				}
			}
			
			//Pawn can move forward only if grid directly in front is not occupied
			if(!inBoard(current_x, current_y - 1)) {
				//do nothing
			}else if(!grid[new_y - 2][new_x - 1].isOccupied()) {
				new_y = current_y - 1;
				//If the pawn reaches the first rank, it has to be promoted
				if(new_y == 1) {
					Move newMove = new Move(current_x, current_y, new_x, new_y, 2);
					unobstructedList.add(newMove);
				}else {
					Move newMove = new Move(current_x, current_y, new_x, new_y, 0);
					unobstructedList.add(newMove);
				}
				new_x = current_x;
				new_y = current_y;
			}
			
			//Pawn can only move one space diagonally forward to capture an enemy piece
			if(!inBoard(new_x - 1, new_y - 1)) {
				//do nothing
			}else if(grid[new_y - 2][new_x - 2].isOccupied() && inBoard(new_x - 1, new_y - 1)) {
				Piece obstructingPiece = board.getPieceFromCoordinate(new_x - 1, new_y - 1, piecesArray);
				if(obstructingPiece.getColor() != this.getColor()) {
					Move newMove = new Move(current_x, current_y, new_x - 1, new_y - 1, 1);
					unobstructedList.add(newMove);
				}
				new_x = current_x;
				new_y = current_y;
			}
			
			if(!inBoard(new_x + 1, new_y - 1)) {
				//do nothing
			}else if(grid[new_y - 2][new_x].isOccupied() && inBoard(new_x + 1, new_y - 1)) {
				Piece obstructingPiece = board.getPieceFromCoordinate(new_x + 1, new_y - 1, piecesArray);
				if(obstructingPiece.getColor() != this.getColor()) {
					Move newMove = new Move(current_x, current_y, new_x + 1, new_y - 1, 1);
					unobstructedList.add(newMove);
				}
				new_x = current_x;
				new_y = current_y;
			}
			
			//checks if a black pawn could en passant
			if(current_y == 4) {
				//En passant for black is only possible if the pawn is on the 4th rank
				//Search to the immediate left and right of the pawn so see if the tiles are occupied
				if(inBoard(current_x - 1, current_y)) {
					if(grid[current_y - 1][current_x - 2].isOccupied()) {
						//If the tile to the left is occupied, check if the piece is an enemy pawn
						Piece piece = board.getPieceFromCoordinate(current_x - 1, current_y, piecesArray);
						if(piece.getColor() != this.getColor() && piece instanceof Pawn) {
							//Check if the move was made immediately on the turn immediately before the current turn
							Turn last_turn = gameLog.get(gameLog.size() - 1);
							Move last_move = last_turn.getMove();
							if(last_move.getNew_x() == last_move.getCurrent_x() && last_move.getNew_y() == last_move.getCurrent_y() + 2) {
								Move newMove = new Move(current_x, current_y, current_x - 1, current_y - 1, 3);
								unobstructedList.add(newMove);
							}
						}
					}
				}
				
				if(inBoard(current_x + 1, current_y)) {
					if(grid[current_y - 1][current_x].isOccupied()) {
						//If the tile to the right is occupied, check if the piece is an enemy pawn
						Piece piece = board.getPieceFromCoordinate(current_x + 1, current_y, piecesArray);
						if(piece.getColor() != this.getColor() && piece instanceof Pawn) {
							//Check if the move was made immediately on the turn immediately before the current turn
							Turn last_turn = gameLog.get(gameLog.size() - 1);
							Move last_move = last_turn.getMove();
							if(last_move.getNew_x() == last_move.getCurrent_x() && last_move.getNew_y() == last_move.getCurrent_y() + 2) {
								Move newMove = new Move(current_x, current_y, current_x + 1, current_y - 1, 3);
								unobstructedList.add(newMove);
							}
						}
					}
				}
			}
		}
		
		return unobstructedList;
	}

	@Override
	public ArrayList<Move> getValidMoves(Tile[][] grid, Board board, Piece[] piecesArray,
			ArrayList<Move> unfilteredList, ArrayList<Turn> gameLog) {
		// TODO Auto-generated method stub
		ArrayList<Move> validMovesList = new ArrayList<Move>();
		for(int i=0; i<unfilteredList.size(); i++) {
			Move move = unfilteredList.get(i);
			int check = board.resultInCheck(grid, piecesArray, move, gameLog);
			if(this.getColor() == 0) {
				//If you are playing as white, you can't make a move such that the white king will be checked
				if(check != 1) {
					validMovesList.add(move);
				}else {
					continue;
				}
			}else if(this.getColor() == 1) {
				//If you are playing as black, you can't make a move such that the black king will be checked
				if(check != 2) {
					validMovesList.add(move);
				}else {
					continue;
				}
			}
		}
		return validMovesList;
	}

}
