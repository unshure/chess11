package pieces;

import java.util.ArrayList;

import game.Board;
import game.Tile;
import game.Turn;

//Special_1 or move_type = 2 is defined as castling king-side
//Special_2 or move_type = 3 is defined as castling queen-side
/** Represents a King Piece
 * @author	Nick Clegg
 * @author	Kevin Wu
 * @version	1.0
 */
public class King extends Piece {
	
	boolean moved = false;

	@Override
	public void move(Tile[][] chessBoard, Piece[] piecesArray, Move move) {
		// TODO Auto-generated method stub
		int current_x = move.getCurrent_x();
		int current_y = move.getCurrent_y();
		
		for(int i=0; i<piecesArray.length; i++) {
			if(piecesArray[i] == null) {
				continue;
			}else if(piecesArray[i].getX_coordinate() == current_x && piecesArray[i].getY_coordinate() == current_y) {
				int new_x = move.getNew_x();
				int new_y = move.getNew_y();
				piecesArray[i].setX_coordinate(new_x);
				piecesArray[i].setY_coordinate(new_y);
				chessBoard[current_y - 1][current_x - 1].setOccupied(false);
				chessBoard[new_y - 1][new_x - 1].setOccupied(true);
				break;
			}
		}
		this.setMoved(true);
	}

	@Override
	public void capture(Tile[][] chessBoard, Piece[] piecesArray, Move move) {
		// TODO Auto-generated method stub
		
		int new_x = move.getNew_x();
		int new_y = move.getNew_y();
		
		for(int i=0; i<piecesArray.length; i++) {
			if(piecesArray[i] == null) {
				continue;
			}else if(piecesArray[i].getX_coordinate() == new_x && piecesArray[i].getY_coordinate() == new_y) {
				piecesArray[i] = null;
				break;
			}
		}
		
		int current_x = move.getCurrent_x();
		int current_y = move.getCurrent_y();
		
		for(int i=0; i<piecesArray.length; i++) {
			if(piecesArray[i] == null) {
				continue;
			}else if(piecesArray[i].getX_coordinate() == current_x && piecesArray[i].getY_coordinate() == current_y) {
				piecesArray[i].setX_coordinate(new_x);
				piecesArray[i].setY_coordinate(new_y);
				chessBoard[current_y - 1][current_x - 1].setOccupied(false);
				break;
			}
		}
		this.setMoved(true);
	}
	
	/** Castling king-side
	 * 
	 * @param chessBoard Current Board
	 * @param piecesArray array of all pieces
	 * @param move Current move being attempted
	 */
	public void castle_short(Tile[][] chessBoard, Piece[] piecesArray, Move move) {
		for(int i=0; i<piecesArray.length; i++) {
			if(piecesArray[i] == null) {
				continue;
			}
			if(piecesArray[i].getX_coordinate() == 8 && piecesArray[i] instanceof Rook && this.getColor() == piecesArray[i].getColor()) {
				//Moving the King
				chessBoard[this.getY_coordinate() - 1][this.getX_coordinate() - 1].setOccupied(false);
				this.setX_coordinate(7);
				chessBoard[this.getY_coordinate() - 1][this.getX_coordinate() - 1].setOccupied(true);
				
				//Moving the Rook
				chessBoard[piecesArray[i].getY_coordinate() - 1][piecesArray[i].getX_coordinate() - 1].setOccupied(false);
				piecesArray[i].setX_coordinate(6);
				chessBoard[piecesArray[i].getY_coordinate() - 1][piecesArray[i].getX_coordinate() - 1].setOccupied(true);
			}
		}
		this.setMoved(true);
	}
	
	/** Castling queen-side
	 * 
	 * @param chessBoard Current Board
	 * @param piecesArray array of all pieces
	 * @param move Current move bieng attempted
	 */
	public void castle_long(Tile[][] chessBoard, Piece[] piecesArray, Move move) {
		for(int i=0; i<piecesArray.length; i++) {
			if(piecesArray[i] == null) {
				continue;
			}
			if(piecesArray[i].getX_coordinate() == 1 && piecesArray[i] instanceof Rook && this.getColor() == piecesArray[i].getColor()) {
				//Moving the King
				chessBoard[this.getY_coordinate() - 1][this.getX_coordinate() - 1].setOccupied(false);
				this.setX_coordinate(3);
				chessBoard[this.getY_coordinate() - 1][this.getX_coordinate() - 1].setOccupied(true);
				
				//Moving the Rook
				chessBoard[piecesArray[i].getY_coordinate() - 1][piecesArray[i].getX_coordinate() - 1].setOccupied(false);
				piecesArray[i].setX_coordinate(4);
				chessBoard[piecesArray[i].getY_coordinate() - 1][piecesArray[i].getX_coordinate() - 1].setOccupied(true);
			}
		}
		this.setMoved(true);
	}
	
	public boolean inBoard(int x_coordinate, int y_coordinate) {
		boolean inBounds = true;
		if(x_coordinate < 1 || x_coordinate > 8 || y_coordinate < 1 || y_coordinate > 8) {
			inBounds = false;
		}
		return inBounds;
	}

	@Override
	public ArrayList<Move> getPossibleMoves(Tile[][] grid, Board board, Piece[] piecesArray, ArrayList<Turn> gameLog) {
		// TODO Auto-generated method stub
		
		ArrayList<Move> unobstructedList = new ArrayList<Move>();
		
		int current_x = this.getX_coordinate();
		int current_y = this.getY_coordinate();
		
		int new_x = current_x;
		int new_y = current_y;
		
		//Move one space top
		if(!inBoard(current_x, current_y + 1)) {
			//If out of bounds, do nothing
		}else if(!grid[new_y][new_x - 1].isOccupied()) {
			Move newMove = new Move(current_x, current_y, new_x, new_y + 1, 0);
			unobstructedList.add(newMove);
		}else {
			//Can only move to obstructing piece's spot to capture
			Piece obstructingPiece = board.getPieceFromCoordinate(new_x, new_y + 1, piecesArray);
			if(obstructingPiece.getColor() != this.getColor()) {
				Move newMove = new Move(current_x, current_y, new_x, new_y + 1, 1);
				unobstructedList.add(newMove);
			}
		}
		
		//Move one space top right
		if(!inBoard(current_x + 1, current_y + 1)) {
			//If out of bounds, do nothing
		}else if(!grid[new_y][new_x].isOccupied()) {
			Move newMove = new Move(current_x, current_y, new_x + 1, new_y + 1, 0);
			unobstructedList.add(newMove);
		}else {
			//Can only move to obstructing piece's spot to capture
			Piece obstructingPiece = board.getPieceFromCoordinate(new_x + 1, new_y + 1, piecesArray);
			if(obstructingPiece.getColor() != this.getColor()) {
				Move newMove = new Move(current_x, current_y, new_x + 1, new_y + 1, 1);
				unobstructedList.add(newMove);
			}
		}
		
		//Move one space right
		if(!inBoard(current_x + 1, current_y)) {
			//If out of bounds, do nothing
		}else if(!grid[new_y - 1][new_x].isOccupied()) {
			Move newMove = new Move(current_x, current_y, new_x + 1, new_y, 0);
			unobstructedList.add(newMove);
		}else {
			//Can only move to obstructing piece's spot to capture
			Piece obstructingPiece = board.getPieceFromCoordinate(new_x + 1, new_y, piecesArray);
			if(obstructingPiece.getColor() != this.getColor()) {
				Move newMove = new Move(current_x, current_y, new_x + 1, new_y, 1);
				unobstructedList.add(newMove);
			}
		}
		
		//Move one space bottom right
		if(!inBoard(current_x + 1, current_y - 1)) {
			//If out of bounds, do nothing
		}else if(!grid[new_y - 2][new_x].isOccupied()) {
			Move newMove = new Move(current_x, current_y, new_x + 1, new_y - 1, 0);
			unobstructedList.add(newMove);
		}else {
			//Can only move to obstructing piece's spot to capture
			Piece obstructingPiece = board.getPieceFromCoordinate(new_x + 1, new_y - 1, piecesArray);
			if(obstructingPiece.getColor() != this.getColor()) {
				Move newMove = new Move(current_x, current_y, new_x + 1, new_y - 1, 1);
				unobstructedList.add(newMove);
			}
		}
		
		
		//Move one space bottom
		if(!inBoard(current_x, current_y - 1)) {
			//If out of bounds, do nothing
		}else if(!grid[new_y - 2][new_x - 1].isOccupied()) {
			Move newMove = new Move(current_x, current_y, new_x, new_y - 1, 0);
			unobstructedList.add(newMove);
		}else {
			//Can only move to obstructing piece's spot to capture
			Piece obstructingPiece = board.getPieceFromCoordinate(new_x, new_y - 1, piecesArray);
			if(obstructingPiece.getColor() != this.getColor()) {
				Move newMove = new Move(current_x, current_y, new_x, new_y - 1, 1);
				unobstructedList.add(newMove);
			}
		}
		
		
		//Move one space bottom left
		if(!inBoard(current_x - 1, current_y - 1)) {
			//If out of bounds, do nothing
		}else if(!grid[new_y - 2][new_x - 2].isOccupied()) {
			Move newMove = new Move(current_x, current_y, new_x - 1, new_y - 1, 0);
			unobstructedList.add(newMove);
		}else {
			//Can only move to obstructing piece's spot to capture
			Piece obstructingPiece = board.getPieceFromCoordinate(new_x - 1, new_y - 1, piecesArray);
			if(obstructingPiece.getColor() != this.getColor()) {
				Move newMove = new Move(current_x, current_y, new_x - 1, new_y - 1, 1);
				unobstructedList.add(newMove);
			}
		}
		
		//Move one space left
		if(!inBoard(current_x - 1, current_y)) {
			//If out of bounds, do nothing
		}else if(!grid[new_y - 1][new_x - 2].isOccupied()) {
			Move newMove = new Move(current_x, current_y, new_x - 1, new_y, 0);
			unobstructedList.add(newMove);
		}else {
			//Can only move to obstructing piece's spot to capture
			Piece obstructingPiece = board.getPieceFromCoordinate(new_x - 1, new_y, piecesArray);
			if(obstructingPiece.getColor() != this.getColor()) {
				Move newMove = new Move(current_x, current_y, new_x - 1, new_y, 1);
				unobstructedList.add(newMove);
			}
		}
		
		//Move one space top left
		if(!inBoard(current_x - 1, current_y + 1)) {
			//If out of bounds, do nothing
		}else if(!grid[new_y][new_x - 2].isOccupied()) {
			Move newMove = new Move(current_x, current_y, new_x - 1, new_y + 1, 0);
			unobstructedList.add(newMove);
		}else {
			//Can only move to obstructing piece's spot to capture
			Piece obstructingPiece = board.getPieceFromCoordinate(new_x - 1, new_y + 1, piecesArray);
			if(obstructingPiece.getColor() != this.getColor()) {
				Move newMove = new Move(current_x, current_y, new_x - 1, new_y + 1, 1);
				unobstructedList.add(newMove);
			}
		}
		
		//Checks if castling short is allowed
		Rook white_kingside_rook = (Rook) piecesArray[9];
		Rook black_kingside_rook = (Rook) piecesArray[25];
		if(white_kingside_rook == null) {
			//do nothing if the white kingside rook is taken
		}else if(this.getColor() == white_kingside_rook.getColor()) {
			//This branch denotes white king
			if(white_kingside_rook.isMoved() == false && this.isMoved() == false && !grid[0][5].isOccupied() && !grid[0][6].isOccupied()) {
				//Can only castle if both the king and the rook have not been moved and if space between is unoccupied
				int check = board.inCheck(grid, piecesArray, gameLog);
				if(check != 1) {
					//Can only castle if king is currently not in check
					//Check if any opposing pieces are attack the squares the king will pass through
					//If any piece is attacking those squares, you cannot castle
					boolean attackedPath = false;
					for(int i=16; i<31; i++) {
						if(piecesArray[i] == null) {
							continue;
						}
						ArrayList<Move> movesList = piecesArray[i].getPossibleMoves(grid, board, piecesArray, gameLog);
						for(int j=0; j<movesList.size(); j++) {
							Move move = movesList.get(j);
							if((move.getNew_x() == 6 || move.getNew_x() == 7) && move.getNew_y() == 1) {
								attackedPath = true;
								break;
							}
						}
						if(attackedPath == true) break;
					}
					if(attackedPath == false) {
						Move newMove = new Move(current_x, current_y, new_x + 2,  new_y, 2);
						unobstructedList.add(newMove);
					}
				}
			}
		}else if(black_kingside_rook == null) {
			//If black rook is taken, do nothing
		}else if(this.getColor() == black_kingside_rook.getColor()) {
			//This branch denotes black king
			if(black_kingside_rook.isMoved() == false && this.isMoved() == false && !grid[7][5].isOccupied() && !grid[7][6].isOccupied()) {
				//Can only castle if both the king and the rook have not been moved and if space between is unoccupied
				int check = board.inCheck(grid, piecesArray, gameLog);
				if(check != 2) {
					//Can only castle if king is currently not in check
					//Check if any opposing pieces are attack the squares the king will pass through
					//If any piece is attacking those squares, you cannot castle
					boolean attackedPath = false;
					for(int i=0; i<15; i++) {
						if(piecesArray[i] == null) {
							continue;
						}
						ArrayList<Move> movesList = piecesArray[i].getPossibleMoves(grid, board, piecesArray, gameLog);
						for(int j=0; j<movesList.size(); j++) {
							Move move = movesList.get(j);
							if((move.getNew_x() == 6 || move.getNew_x() == 7) && move.getNew_y() == 8) {
								attackedPath = true;
								break;
							}
						}
						if(attackedPath == true) break;
					}
					if(attackedPath == false) {
						Move newMove = new Move(current_x, current_y, new_x + 2,  new_y, 2);
						unobstructedList.add(newMove);
					}
				}
			}
		}
		
		//Checks if castling long is allowed
		Rook white_queenside_rook = (Rook) piecesArray[8];
		Rook black_queenside_rook = (Rook) piecesArray[24];
		if(white_queenside_rook == null) {
			//If white queenside rook has been taken do thing
		}else if(this.getColor() == white_queenside_rook.getColor()) {
			//This branch denotes white king
			if(white_queenside_rook.isMoved() == false && this.isMoved() == false && !grid[0][1].isOccupied() && !grid[0][2].isOccupied() && !grid[0][3].isOccupied()) {
				//Can only castle if both the king and the rook have not been moved and if space between is unoccupied
				int check = board.inCheck(grid, piecesArray, gameLog);
				if(check != 1) {
					//Can only castle if king is currently not in check
					//Check if any opposing pieces are attack the squares the king will pass through
					//If any piece is attacking those squares, you cannot castle
					boolean attackedPath = false;
					for(int i=16; i<31; i++) {
						if(piecesArray[i] == null) {
							continue;
						}
						ArrayList<Move> movesList = piecesArray[i].getPossibleMoves(grid, board, piecesArray, gameLog);
						for(int j=0; j<movesList.size(); j++) {
							Move move = movesList.get(j);
							if((move.getNew_x() == 3 || move.getNew_x() == 4) && move.getNew_y() == 1) {
								attackedPath = true;
								break;
							}
						}
						if(attackedPath == true) break;
					}
					if(attackedPath == false) {
						Move newMove = new Move(current_x, current_y, new_x - 2,  new_y, 3);
						unobstructedList.add(newMove);
					}
				}
			}
		}else if(black_queenside_rook == null) {
			//If black queenside rook has been taken do nothing
		}else if(this.getColor() == black_queenside_rook.getColor()) {
			//This branch denotes black king
			if(black_queenside_rook.isMoved() == false && this.isMoved() == false && !grid[7][1].isOccupied() && !grid[7][2].isOccupied() && !grid[7][3].isOccupied()) {
				//Can only castle if both the king and the rook have not been moved and if space between is unoccupied
				int check = board.inCheck(grid, piecesArray, gameLog);
				if(check != 2) {
					//Can only castle if king is currently not in check
					//Check if any opposing pieces are attack the squares the king will pass through
					//If any piece is attacking those squares, you cannot castle
					boolean attackedPath = false;
					for(int i=0; i<15; i++) {
						if(piecesArray[i] == null) {
							continue;
						}
						ArrayList<Move> movesList = piecesArray[i].getPossibleMoves(grid, board, piecesArray, gameLog);
						for(int j=0; j<movesList.size(); j++) {
							Move move = movesList.get(j);
							if((move.getNew_x() == 3 || move.getNew_x() == 4) && move.getNew_y() == 8) {
								attackedPath = true;
								break;
							}
						}
						if(attackedPath == true) break;
					}
					if(attackedPath == false) {
						Move newMove = new Move(current_x, current_y, new_x - 2,  new_y, 3);
						unobstructedList.add(newMove);
					}
				}
			}
		}
		return unobstructedList;
	}

	public boolean isMoved() {
		return moved;
	}

	public void setMoved(boolean moved) {
		this.moved = moved;
	}

	@Override
	public ArrayList<Move> getValidMoves(Tile[][] grid, Board board, Piece[] piecesArray,
			ArrayList<Move> unfilteredList, ArrayList<Turn> gameLog) {
		// TODO Auto-generated method stub
		ArrayList<Move> validMovesList = new ArrayList<Move>();
		for(int i=0; i<unfilteredList.size(); i++) {
			Move move = unfilteredList.get(i);
			int check = board.resultInCheck(grid, piecesArray, move, gameLog);
			if(this.getColor() == 0) {
				//If you are playing as white, you can't make a move such that the white king will be checked
				if(check != 1) {
					validMovesList.add(move);
				}else {
					continue;
				}
			}else if(this.getColor() == 1) {
				//If you are playing as black, you can't make a move such that the black king will be checked
				if(check != 2) {
					validMovesList.add(move);
				}else {
					continue;
				}
			}
		}
		return validMovesList;
	}

}
