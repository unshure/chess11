package chess;

import game.Game;
/**
 * @author	Nick Clegg
 * @author	Kevin Wu
 * @version	1.0
 */
public class Chess {
	
	/**
	 * Main method to start the game
	 * @param	args	not used for this application
	 */
	public static void main(String[] args) {
		Game game = new Game();
		game.playGame();
	}
	
}
