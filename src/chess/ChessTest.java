package chess;

import java.util.ArrayList;

import game.Board;
import game.Tile;
import pieces.Move;
import pieces.Piece;

@SuppressWarnings("unused")
public class ChessTest {
	
	public static void main(String[] args) {
		/*Board board = new Board();
		
		//Create an 8x8 Tile array and set that as the chess board
		Tile[][] chessBoard = new Tile[8][8];
		for(int i=0; i<8; i++) {
			for(int j=0; j<8; j++) {
				chessBoard[i][j] = new Tile();
			}
		}
		board.setChessBoard(chessBoard);
		
		//Create array to keep track of pieces and locations
		Piece[] piecesArray = new Piece[32];
		board.setPiecesArray(piecesArray);
		
		board.initializeGame(chessBoard, piecesArray);
		
		//Testing to see if program could get list of valid moves
		piecesArray[29].setX_coordinate(3);
		piecesArray[29].setY_coordinate(3);
		chessBoard[7][5].setOccupied(false);
		chessBoard[2][2].setOccupied(true);
		
		piecesArray[27].setX_coordinate(6);
		piecesArray[27].setY_coordinate(4);
		chessBoard[7][6].setOccupied(false);
		chessBoard[3][5].setOccupied(true);
		
		piecesArray[6].setX_coordinate(7);
		piecesArray[6].setY_coordinate(4);
		chessBoard[1][6].setOccupied(false);
		chessBoard[3][6].setOccupied(true);
		
		piecesArray[4].setX_coordinate(5);
		piecesArray[4].setY_coordinate(4);
		chessBoard[1][4].setOccupied(false);
		chessBoard[3][4].setOccupied(true);
		
		piecesArray[30].setX_coordinate(5);
		piecesArray[30].setY_coordinate(2);
		chessBoard[7][3].setOccupied(false);
		chessBoard[1][4].setOccupied(true);
		
		piecesArray[19].setX_coordinate(4);
		piecesArray[19].setY_coordinate(5);
		chessBoard[6][3].setOccupied(false);
		chessBoard[4][3].setOccupied(true);
		
		board.printBoard(chessBoard, piecesArray);
		
		/*ArrayList<Move> movesList = piecesArray[15].getPossibleMoves(chessBoard, board, piecesArray);
		for(int i=0; i<movesList.size(); i++) {
			Move possibleMove = movesList.get(i);
			System.out.println(possibleMove.getNew_x() + " " + possibleMove.getNew_y() + " " + possibleMove.getMove_type());
		}*/
		
		/*ArrayList<Move> unfilteredMovesList = piecesArray[15].getPossibleMoves(chessBoard, board, piecesArray);
		ArrayList<Move> validMovesList = piecesArray[15].getValidMoves(chessBoard, board, piecesArray, unfilteredMovesList);
		for(int i=0; i<validMovesList.size(); i++) {
			Move validMove = validMovesList.get(i);
			System.out.println(validMove.getNew_x() + " " + validMove.getNew_y() + " " + validMove.getMove_type());
		}*/
		
		/*ArrayList<Move> validMovesList = new ArrayList<Move>();
		for(int i=0; i<16; i++) {
			ArrayList<Move> unfilteredMovesList = piecesArray[i].getPossibleMoves(chessBoard, board, piecesArray);
			ArrayList<Move> validMovesForPiece = piecesArray[i].getValidMoves(chessBoard, board, piecesArray, unfilteredMovesList);
			validMovesList.addAll(validMovesForPiece);
		}
		
		for(int i=0; i<validMovesList.size(); i++) {
			Move validMove = validMovesList.get(i);
			System.out.println(validMove.getCurrent_x() + " " + validMove.getCurrent_y() + " " + 
			validMove.getNew_x() + " " + validMove.getNew_y() + " " + validMove.getMove_type());
		}*/
		
		//Testing if checking if any of the kings are in check works properly
		/*piecesArray[15].setX_coordinate(5);
		piecesArray[15].setY_coordinate(4);
		chessBoard[0][4].setOccupied(false);
		chessBoard[3][4].setOccupied(true);
		
		piecesArray[24].setX_coordinate(1);
		piecesArray[24].setY_coordinate(4);
		chessBoard[7][0].setOccupied(false);
		chessBoard[3][0].setOccupied(true);
		
		piecesArray[2].setX_coordinate(3);
		piecesArray[2].setY_coordinate(4);
		chessBoard[1][2].setOccupied(false);
		chessBoard[3][2].setOccupied(true);
		
		/*piecesArray[31].setX_coordinate(2);
		piecesArray[31].setY_coordinate(5);
		chessBoard[7][4].setOccupied(false);
		chessBoard[4][1].setOccupied(true);
		
		board.printBoard(chessBoard, piecesArray);
		
		System.out.println(board.inCheck(chessBoard, piecesArray));
		
		Move someMove = new Move(3, 4, 3, 5, 0);
		int check = board.resultInCheck(chessBoard, piecesArray, someMove);
		System.out.println(check);
		
		System.out.println(board.inCheck(chessBoard, piecesArray));*/
		
		//Testing more stuff
	}

}
